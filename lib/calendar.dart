import 'package:axios_calendar/Model/CalendarModel.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'Model/CalendarModel.dart';

class AxiosCalendar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CalendarModel>(
      create: (_) => CalendarModel(),
      child: CalendarView(),
    );
  }
}

class CalendarView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final calendarModel = Provider.of<CalendarModel>(context);
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: <Widget>[
            calendarNav(calendarModel.getCurrentYear(),
                calendarModel.getCurrentMonth(), calendarModel),
            calendarHead(),
            calendarBody(calendarModel.getMonthData()),
          ],
        ),
      ),
    );
  }
}

List nepaliMonths = [
  "",
  "बैशाख",
  "जेठ",
  "असार",
  "श्रावण",
  "भदौ",
  "आश्विन",
  "कार्तिक",
  "मंसिर",
  "पुष",
  "माघ",
  "फाल्गुन",
  "चैत्र"
];

calendarNav(curYear, curMonth, calendarModel) {
  return Container(
    color: Colors.blueGrey[50],
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        IconButton(
          icon: CircleAvatar(
              backgroundColor: Colors.green,
              child: Icon(
                Icons.arrow_left,
                color: Colors.white,
              )),
          onPressed: () {
            calendarModel.getPreviousMonthData();
          },
        ),
        Text(
          curYear.toString() + " " + nepaliMonths[curMonth],
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18,
              color: Colors.black87,
              fontFamily: 'Kalimati',
              package: 'axios_calendar'),
        ),
        IconButton(
          icon: CircleAvatar(
              backgroundColor: Colors.green,
              child: Icon(
                Icons.arrow_right,
                color: Colors.white,
              )),
          onPressed: () {
            calendarModel.getNextMonthData();
          },
        ),
      ],
    ),
  );
}

calendarHead() {
  return GridView(
    shrinkWrap: true,
    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 7),
    children: <Widget>[
      dayIndicator("आइत", Colors.green),
      dayIndicator("सोम", Colors.green),
      dayIndicator("मंगल", Colors.green),
      dayIndicator("बुध", Colors.green),
      dayIndicator("बिहि", Colors.green),
      dayIndicator("शुक्र", Colors.green),
      dayIndicator("शनि", Colors.red)
    ],
  );
}

dayIndicator(text, color) {
  return Center(
      child: Text(
    text,
    style: TextStyle(color: color, fontWeight: FontWeight.bold, fontSize: 16),
  ));
}

calendarBody(data) {
  return GridView.builder(
    physics: NeverScrollableScrollPhysics(),
    shrinkWrap: true,
    itemCount: data.length,
    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
      childAspectRatio: 1.2,
      crossAxisCount: 7,
    ),
    itemBuilder: (BuildContext context, int index) {
      return singleDateWidget(data[index], index + 1);
    },
  );
}

singleDateWidget(day, index) {
  bool isactive = day.active;
  bool isSaturday = index % 7 == 0;

  var now = new DateTime.now();
  String englishDate =
      day.englishYear + '-' + day.englishMonth + '-' + day.englishDate;
  bool isToday = DateFormat("yyyy-MM-dd").format(now) == englishDate;

  // if day is not in the month then, color greyed out, else if day is Saturday color is red, else color is black
  Color dayColor = !isactive
      ? Colors.black12
      : isSaturday ? Colors.red : isToday ? Colors.white : Colors.black;

  return Container(
    color: isToday ? Colors.green : Colors.transparent,
    child: Center(
      child: Text(
        day.gate.toString(),
        style: TextStyle(
            fontSize: 16,
            color: dayColor,
            fontWeight: FontWeight.bold,
            fontFamily: 'Kalimati',
            package: 'axios_calendar'),
      ),
    ),
  );
}
