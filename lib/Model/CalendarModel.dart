import 'package:axios_calendar/Model/MonthDataModel.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';

class CalendarModel with ChangeNotifier {
  CalendarModel() {
    getCalendarData(null, null);
  }

  getPreviousMonthData() {
    getCalendarData(getPreviousMonth(), getPreviousYear());
  }

  getNextMonthData() {
    getCalendarData(getNextMonth(), getNextYear());
  }

  int _currentYear = 0;
  int _currentMonth = 0;
  int _previousYear = 0;
  int _previousMonth = 0;
  int _nextYear = 0;
  int _nextMonth = 0;
  List<MonthDataModel> monthDataList = [];

  getCurrentYear() => _currentYear;
  getCurrentMonth() => _currentMonth;
  getPreviousYear() => _previousYear;
  getPreviousMonth() => _previousMonth;
  getNextYear() => _nextYear;
  getNextMonth() => _nextMonth;
  getMonthData() => monthDataList;

  void getCalendarData(prevMonth, prevYear) async {
    String url = prevMonth == null || prevYear == null
        ? "https://rms.nivid.app/dashboard/getMonthCalendarApi"
        : "https://rms.nivid.app/dashboard/getMonthCalendarApi?month=" +
            prevMonth.toString() +
            "&year=" +
            prevYear.toString();
    try {
      Response response = await Dio().get(url);
      addToDateModel(response.data);
    } catch (e) {
      print(e);
    }
  }

  addToDateModel(data) {
    monthDataList = [];
    _currentYear = data["curYear"];
    _currentMonth = data["curMonth"];
    _previousMonth = data["prevMonth"];
    _previousYear = data["prevYear"];
    _nextMonth = data["nextMonth"];
    _nextYear = data["nextYear"];
    data["monthdata"].forEach((data) => {
          monthDataList.add(MonthDataModel(
              active: data['active'],
              dayid: data['dayid'],
              year: data['year'],
              nepaliMonth: data['nepaliMonth'],
              gate: data['gate'],
              englishYear: data['englishYear'],
              englishMonth: data['englishMonth'],
              englishDate: data['englishDate'],
              event: data['event'],
              eventColour: data['eventColour']))
        });
    notifyListeners();
  }
}
