class MonthDataModel {
  bool active = false;
  int dayid = 0;
  int year = 0;
  int nepaliMonth = 0;
  int gate = 0;
  String englishYear = '';
  String englishMonth = '';
  String englishDate = '';
  String event = '';
  String eventColour = '';

  MonthDataModel({
    this.active,
    this.dayid,
    this.englishDate,
    this.englishMonth,
    this.englishYear,
    this.event,
    this.eventColour,
    this.gate,
    this.nepaliMonth,
    this.year,
  });
}
